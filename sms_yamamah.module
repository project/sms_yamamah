<?php

/**
 * @file
 * Adds support for sending SMS messages using the yamamah.com gateway.
 */

/**
 * Implements hook_gateway_info().
 */
function sms_yamamah_gateway_info() {
  return array(
    'yamamah' => array(
      'name' => 'Yamamah',
      'configure form' => 'sms_yamamah_admin_form',
      'send' => 'sms_yamamah_send',
      'send form' => 'sms_yamamah_send_form',
    ),
  );
}

/**
 * Configuration form for gateway module.
 */
function sms_yamamah_admin_form($configuration) {

  $form['sms_yamamah_sender'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender'),
    '#description' => t('The sender (tagname) name of your !yamamah.com gateway account.', array(
      '!yamamah.com' => '<a target="_blank" href="http://yamamah.com">yamamah.com</a>',
    )),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => (isset($configuration) && isset($configuration['sms_yamamah_sender'])) ? $configuration['sms_yamamah_sender'] : '',
  );

  $form['sms_yamamah_user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#description' => t('The username of your !yamamah.com gateway account.', array(
      '!yamamah.com' => '<a target="_blank" href="http://yamamah.com">yamamah.com</a>',
    )),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => (isset($configuration) && isset($configuration['sms_yamamah_user'])) ? $configuration['sms_yamamah_user'] : '',
  );

  $form['sms_yamamah_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('The password of your !yamamah.com gateway account.', array(
      '!yamamah.com' => '<a target="_blank" href="http://yamamah.com">yamamah.com</a>',
    )),
    '#size' => 30,
    '#maxlength' => 64,
    '#default_value' => (isset($configuration) && isset($configuration['sms_yamamah_password'])) ? $configuration['sms_yamamah_password'] : '',
  );

  return $form;
}

/**
 * Callback for sending messages.
 */
function sms_yamamah_send($number, $message, $options) {

  $gateway = sms_gateways('gateway', 'yamamah');
  $config = $gateway['configuration'];

  $url = "https://api1.yamamah.com/SendSMSV3";
  $sendr = $config['sms_yamamah_sender'];

  $string_to_post = array(
    'Username' => $config['sms_yamamah_user'],
    'Password' => $config['sms_yamamah_password'],
    'Tagname'  => $sendr,
    'RecepientNumber'  => $number,
    'Message'  => $message,
    'VariableList' => "0",
    'SendDateTime' => "0",
    'EnableDR' => true,
    'SentMessageID' => true,
  );

  $header = array(
    'Content-Type: application/json',
    'Host: api1.yamamah.com'
  );
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_TIMEOUT, 0);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($string_to_post));
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

  $result = curl_exec($ch);
  curl_close($ch);

  return $result;
}


/**
 * Returns custom additions to be added to the send forms.
 */
function sms_yamamah_send_form() {
  $form['country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#multiple' => FALSE,
    '#options' => sms_country_codes(),
    '#default_value' => -1,
  );

  return $form;
}

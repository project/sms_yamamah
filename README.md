CONTENTS OF THIS FILE
---------------------

 * SUMMARY
 * Requirements
 * Installation
 * Configuration
 * Maintainers

-- SUMMARY --

This module provides integration between Yamamah SMS and SMS Framework.

-- REQUIREMENTS --

SMS Framework
https://www.drupal.org/project/smsframework

-- INSTALLATION --

* Install as usual, see:
https://www.drupal.org/documentation/install/modules-themes/modules-7
for further information.


-- CONFIGURATION --

 - Just enable the module, A getway option will be availabe in
 SMS Framework (Gateway configuration).
 - Put your Yamamah username and password in the options you see.

-- Maintainers --
Current maintainers:
* Mohammed Bamlhes (bamlhes) - https://www.drupal.org/u/bamlhes
